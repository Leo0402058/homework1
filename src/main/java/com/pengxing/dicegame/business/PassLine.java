/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.business;

import com.pengxing.dicegame.business.MessageBox;
import com.pengxing.dicegame.data.DiceBean;

/**
 *
 * @author User
 */
public class PassLine{
    DiceBean dicebean = new DiceBean();
    int total = 0;
    int status = 0;
    int count = 0;
    
     public void points(){
        
        String msg = "You have win " + dicebean.getPoint()+" points";
        String title = "Continue!!";
        MessageBox.show(msg,title);
     }
    
     public void matchPointWin(){
        dicebean.setBalance(dicebean.getBetAmount()+dicebean.getBalance());
        String msg = "Your roll match the points " + dicebean.getPoint() + "\n" 
                + "You have win " + dicebean.getBetAmount()+"dollars";
        String title = "Congratulations!!";
        MessageBox.show(msg,title);
     }
    
    
      public void isWin(){
        dicebean.setBalance(dicebean.getBetAmount()+dicebean.getBalance());
        String msg = "You have win " + dicebean.getBetAmount()+"dollars";
        String title = "Congratulations!!";
        MessageBox.show(msg,title);
     }
      
      
     
     public void isLoose(){
        
        dicebean.setBalance(dicebean.getBalance()-dicebean.getBetAmount()); 
        String msg = "You have lose your bet: " + dicebean.getBetAmount()+" dollars"; 
        String title = "Sorry!!";
        MessageBox.show(msg,title);
        
       
     }
     
    
     
     public void isContinue(){
        String msg = "Continue to Roll"; 
        String title = "Continue!!";
        MessageBox.show(msg,title);
     }
     
     
     public int rollContinue(DiceBean db,int total){
         this.dicebean = db;
         
         if(total == dicebean.getPoint()){
            matchPointWin();
            status= 1;
         }else {
            if(total == 7){
            isLoose();
            status= 0;
            }
            else{
                isContinue();
                status= 2; 
            }
 
         
        
         }
         return status;
     }
    public int firstRoll(DiceBean db,int total){
        this.dicebean = db;
        switch(total){
                   case 7: isWin();
                           status= 1;
                           break;
                
                   case 11: isWin();
                           status= 1;
                           break;
                   case 2: isLoose();
                           status= 0;
                           break;
                           
                   case 3: isLoose();
                           status= 0;
                           break;
                   case 12: isLoose();
                            status= 0; 
                            break;
                            
                   default: isContinue();
                            dicebean.setPoint(total);
                            points();
                            status =2;
                            break;
         }
        return status;
    }
//    
//    public int PassLine( DiceBean db, double betAmount, int firstDice, int secondDice){
//       int status = 0;
//       this.dicebean = db;
//       total = firstDice + secondDice;
//       dicebean.setBetAmount(betAmount);
//       if(firstRoll(total)==2){isContinue();status =1;}else{status = 0;}
//     return status;
 //   }   
       
}       
    

