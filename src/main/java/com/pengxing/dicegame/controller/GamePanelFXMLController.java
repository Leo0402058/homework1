/**
 * Sample Skeleton for 'GamePanelFXML.fxml' Controller Class
 */

package com.pengxing.dicegame.controller;

import com.pengxing.dicegame.business.PassLine;
import com.pengxing.dicegame.business.MessageBox;
import com.pengxing.dicegame.business.FieldBet;
import com.pengxing.dicegame.business.Any7;
import com.pengxing.dicegame.data.Context;
import com.pengxing.dicegame.data.DiceBean;
import com.pengxing.dicegame.data.UserBean;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GamePanelFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="flineradion"
    private RadioButton flineradion; // Value injected by FXMLLoader

    @FXML // fx:id="gamemode"
    private ToggleGroup gamemode; // Value injected by FXMLLoader

   @FXML // fx:id="betradion"
    private RadioButton betradion; // Value injected by FXMLLoader

    @FXML // fx:id="any7radio"
    private RadioButton any7radio; // Value injected by FXMLLoader

    @FXML // fx:id="amountbettxt"
    private TextField amountbettxt; // Value injected by FXMLLoader

    @FXML // fx:id="confirmbtn"
    private Button confirmbtn; // Value injected by FXMLLoader

    @FXML // fx:id="cancelbtn"
    private Button cancelbtn; // Value injected by FXMLLoader

    @FXML // fx:id="deposittxt"
    private TextField deposittxt; // Value injected by FXMLLoader

    @FXML // fx:id="depositcomfirm"
    private Button depositcomfirm; // Value injected by FXMLLoader

    @FXML // fx:id="depositcancel"
    private Button depositcancel; // Value injected by FXMLLoader

    @FXML // fx:id="rollGame"
    private AnchorPane rollGame; // Value injected by FXMLLoader

    @FXML // fx:id="rollhandle"
    private Button rollhandle; // Value injected by FXMLLoader

    @FXML // fx:id="playertxt"
    private Label playertxt; // Value injected by FXMLLoader

    @FXML // fx:id="fundtxt"
    private Label fundtxt; // Value injected by FXMLLoader

    @FXML // fx:id="betshowtxt"
    private Label betshowtxt; // Value injected by FXMLLoader

    @FXML // fx:id="imageOne"
    private ImageView imageOne; // Value injected by FXMLLoader

    @FXML // fx:id="imageTwo"
    
   private ImageView imageTwo; // Value injected by FXMLLoader

   DiceBean dicebean ;
   UserBean userbean; //= Context.getInstance().currentUserBean();
   
   //DiceGameFXMLController dicecontrol = new DiceGameFXMLController();
   static int count = 0;
   boolean checker = false;
   double betAmount = 0.0;
   PassLine passline;
   FieldBet fieldbet;
   Any7 any7;
   private double balance;
  
   
   Image imgOne = new Image("/images/1.png");
   Image imgTwo = new Image("/images/2.png");
   Image imgThree = new Image("/images/3.png");
   Image imgFour = new Image("/images/4.png");
   Image imgFive = new Image("/images/5.png");
   Image imgSix = new Image("/images/6.png");
    
    
    
    @FXML
    void cancelBet(ActionEvent event) {
       dicebean.setBalance(dicebean.getBalance()-dicebean.getBetAmount());
       dicebean.setBetAmount(0.0);
       amountbettxt.clear();
       betshowtxt.setText("0.00");
       deposit();
    }

    @FXML
    void confirmBet(ActionEvent event) {
         if(dicebean.getBalance()>0.0){
                if(Double.parseDouble(amountbettxt.getText())>dicebean.getBalance()){
                    lowBalaceWarn();
                    deposit();
                }else{
                     if(Double.parseDouble(amountbettxt.getText())>=10.0){
                        dicebean.setBetAmount(Double.parseDouble(amountbettxt.getText()));
                        betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
                        
                        amountbettxt.clear();
                        play();
                     }else{
                         String msg = "You Need Set At Least 10$ To Start!!";
                         String title = "Bet Too Low!";
                         MessageBox.show(msg, title);
                         deposit();
                         
                     }
                }
          }
    }

    @FXML
    void depositCancel(ActionEvent event) {
        
         dicebean.setBalance(dicebean.getBalance()-Double.parseDouble(deposittxt.getText()));
         
    }

    @FXML
    void depositComfirm(ActionEvent event) {
        
        dicebean.setBalance(Double.parseDouble(deposittxt.getText())+dicebean.getBalance());
        fundtxt.setText(String.valueOf(dicebean.getBalance()));
        deposittxt.clear();
        deposit();
        
    }

    @FXML
    void rollStart(ActionEvent event) {
        if(flineradion.isSelected()){
           
           passline = new PassLine();
           Random rn = new Random();
           dicebean.setDieOne(rn.nextInt(6)+1);
           dicebean.setDieTwo(rn.nextInt(6)+1);
           int total = dicebean.getDieOne()+dicebean.getDieTwo();
           imageChoose();
           
         if(dicebean.getBalance()>=dicebean.getBetAmount()){
           
           if(dicebean.isGameOpen()==false){
             if(passline.firstRoll(dicebean, total)==2){
                 dicebean.setPoint(total);
                 dicebean.setGameOpen(true);
                 play();
             }else{
                 dicebean.setGameOpen(false);
                 dicebean.setBetAmount(0.00);
                 deposit();
                 betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
                 fundtxt.setText(String.valueOf(dicebean.getBalance()));
             }
           }else{
           
                 if(passline.rollContinue(dicebean,total)==2){
                
                    dicebean.setGameOpen(true);
                    play();
                
                 }else{        
                    dicebean.setGameOpen(false);
                    dicebean.setBetAmount(0.0);
                    deposit();
                    betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
                    fundtxt.setText(String.valueOf(dicebean.getBalance()));
             
                }
           }
     
        }
         
           
       }else{ if(betradion.isSelected()){
           fieldbet = new FieldBet();
           
           Random rn = new Random();
           dicebean.setDieOne(rn.nextInt(6)+1);
           dicebean.setDieTwo(rn.nextInt(6)+1);
           int total = dicebean.getDieOne()+dicebean.getDieTwo();
           imageChoose();
           fieldbet.fieldBet(dicebean,total);
           dicebean.setBetAmount(0.0);
           deposit();
           betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
           fundtxt.setText(String.valueOf(dicebean.getBalance()));
           
          
           
           }else {if(any7radio.isSelected()){
           any7 = new Any7();
           Random rn = new Random();
           dicebean.setDieOne(rn.nextInt(6)+1);
           dicebean.setDieTwo(rn.nextInt(6)+1);
           imageChoose();
           int total = dicebean.getDieOne()+dicebean.getDieTwo();
           any7.anySeven(dicebean,total);
           dicebean.setBetAmount(0.0);
           deposit();
           betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
           fundtxt.setText(String.valueOf(dicebean.getBalance()));

           }
          }
        }
    }
    
     public void imageChoose(){


          //Set image sequence for imageView Two
          
          if(dicebean.getDieOne()==1){
              imageOne.setImage(imgOne);
          }else if(dicebean.getDieOne()==2){
              imageOne.setImage(imgTwo);
          }else if(dicebean.getDieOne()==3){
              imageOne.setImage(imgThree);
          }else if(dicebean.getDieOne()==4){
              imageOne.setImage(imgFour);
           }else if(dicebean.getDieOne()==5){
              imageOne.setImage(imgFive);
           }else if(dicebean.getDieOne()==6){
              imageOne.setImage(imgSix);
           }
          
           //Set image sequence for imageView one
            if(dicebean.getDieTwo()==1){
              imageTwo.setImage(imgOne);
          }else if(dicebean.getDieTwo()==2){
              imageTwo.setImage(imgTwo);
          }else if(dicebean.getDieTwo()==3){
              imageTwo.setImage(imgThree);
          }else if(dicebean.getDieTwo()==4){
              imageTwo.setImage(imgFour);
           }else if(dicebean.getDieTwo()==5){
              imageTwo.setImage(imgFive);
           }else if(dicebean.getDieTwo()==6){
              imageTwo.setImage(imgSix);
           }
            
            

          
   }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws CloneNotSupportedException {
         assert flineradion != null : "fx:id=\"flineradion\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert gamemode != null : "fx:id=\"gamemode\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
          assert betradion != null : "fx:id=\"betradion\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert any7radio != null : "fx:id=\"any7radio\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert amountbettxt != null : "fx:id=\"amountbettxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert confirmbtn != null : "fx:id=\"confirmbtn\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert cancelbtn != null : "fx:id=\"cancelbtn\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert deposittxt != null : "fx:id=\"deposittxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert depositcomfirm != null : "fx:id=\"depositcomfirm\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert depositcancel != null : "fx:id=\"depositcancel\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert rollGame != null : "fx:id=\"rollGame\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert rollhandle != null : "fx:id=\"rollhandle\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert playertxt != null : "fx:id=\"playertxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert fundtxt != null : "fx:id=\"fundtxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert betshowtxt != null : "fx:id=\"betshowtxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert imageOne != null : "fx:id=\"imageOne\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert imageTwo != null : "fx:id=\"imageTwo\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        
        
      
    }
    
    public void deposit(){
          rollhandle.setDisable(true);
           confirmbtn.setDisable(false);
           amountbettxt.setDisable(false);
           cancelbtn.setDisable(false);
           depositcomfirm.setDisable(false);
           depositcancel.setDisable(false);
           deposittxt.setDisable(false);
    }

    public void play(){
       rollhandle.setDisable(false);    
           confirmbtn.setDisable(true);
           amountbettxt.setDisable(true);
           cancelbtn.setDisable(false);
           depositcomfirm.setDisable(true);
           depositcancel.setDisable(true);
           deposittxt.setDisable(true);

  }
 
  public void lowBalaceWarn(){
            String msg = "Bet amount should be less than or \n equal to your balance!!";
            String title = "Low Fund Waring!";
            MessageBox.show(msg, title);
  
  }

  public boolean rollChecker(double betAmount, double balance){
        boolean roll = true;
        
        if((balance-betAmount)<0){
       
            String msg = "Balance will be lower than zero!!";
            String title = "Low Fund Waring!";
            MessageBox.show(msg, title);
            roll = false;
        }else{roll = true;}
        
        return roll;
    }
 

  
    
     public void setUserBean(UserBean userbean)throws SQLException {
        this.userbean = userbean;
        fundtxt.setText(String.valueOf(userbean.getPlayAmount()));
        
    }
      public void setDiceBean(DiceBean dicebean)throws SQLException {
        this.dicebean=dicebean;
        dicebean.setBetAmount(10.00);
        betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
        dicebean.setGameOpen(false);
        playertxt.setText(dicebean.getUserName());
        play();
        
    }
      
      
}
