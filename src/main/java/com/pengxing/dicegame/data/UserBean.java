package com.pengxing.dicegame.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Ken Fogel
 */
public class UserBean {

     private StringProperty playerName;
     private StringProperty playPassword;
     private StringProperty playerEmail;
     private DoubleProperty playAmount;
     private boolean gameJudge;
   
    public boolean getGameJudge() {
        return gameJudge;
    }

    public void setGameJudge(boolean gameJudge) {
        this.gameJudge = gameJudge;
    }

    /**
     * Non-default constructor
     *
     * @param userName
     * @param userPassword
     */
    public UserBean(final String playerName, final String playPassword,final String playerEmail,final double playAmount) {
        super();
        this.playerName = new SimpleStringProperty(playerName);
        this.playPassword = new SimpleStringProperty(playPassword);
        this.playerEmail = new SimpleStringProperty(playerEmail);
        this.playAmount = new SimpleDoubleProperty(playAmount);
        
    }

    /**
     * Default Constructor
     */
    public UserBean() {
        this("", "","",0.0);
    }

     public String getPlayerName() {
        return playerName.get();
    }

    public void setPlayerName(String playerName) {
        this.playerName.set(playerName);
    }
    
    public final StringProperty playerNameProperty(){
    
        return playerName;
    }
    
    public String getPlayerEmail() {
        return playerEmail.get();
    }

    public void setPlayerEmail(String playerEmail) {
        this.playerEmail.set(playerEmail);
    }
    public final StringProperty playerEmailProperty(){
    
        return playerEmail;
        
    }
    
     public String getplayPassword() {
        return playerName.get();
    }

    public void setplayPassword(String playerName) {
        this.playerName.set(playerName);
    }
    
    public final StringProperty playPasswordProperty(){
    
        return playerName;
    }
    
    public Double getPlayAmount() {
        return playAmount.get();
        
    }
    

    public void setPlayAmount(Double playAmount) {
        this.playAmount.set(playAmount);
    }
     
    public final DoubleProperty playAmountProperty(){
    
        return playAmount;
    }
    

    @Override
    public String toString() {
        return "UserBean{" + "playerName=" + playerName.get() + ", playPassword=" + playPassword.get() + ", playerEmail=" + playerEmail.get() + ", playAmount=" + playAmount.getName() + '}';
    }
   
 }